# Nom du projet

Ce projet de recherche vise à étudier les dynamiques des
opinions politiques au sein des familles. Au sens large, une opinion politique
se caractérise par un sentiment, jugement qu’un individu manifeste sur la
politique qui elle est « relative à une conception particulière du gouvernement ».

De façon très simplifiée, on vise à mesurer le taux de transmission d’opinion politique au sein d’une même famille de façon microscopique (en utilisant un nombre réduit de familles). Pour ce faire, nous avons conçu un
modèle mettant en interaction des individus partageant des liens de parenté (soit par alliance, soit par filiation).


## English version

This research project aims to study opinion dynamics within families. 
In this context, a political opinion is characterized by a feeling, and/or a judgment a person has/makes on
politics (as a global concept) which "relates to a specific idea of government".

Simply, we aim to microscopicaly mesure political opinion transmission rates within (few) families.
To achieve this goal, we've conceived a model which allows us to oversee interactions between individuals
some of which are related (regardless of wether it is by blood or by way of marriage).


## Présentation de l'équipe

- A. Sarre
- K. Bennouna
- T. Claudia
- M. Chibkouni

## Description synthétique du projet

**Problématique :** 

Quelles sont les dynamiques d’opinions politiques au sein d’une famille?

**Hypothèse principale :**

Au cours du temps, les opinions politiques au sein d’une famille tendent à converger.

**Hypothèses secondaires :** 

Quel est l’impact de la taille de la famille sur la dynamique des opinions politiques intra et extra-familiales?

**Objectifs :**

Étudier l’évolution de l’opinion politique d’une famille au cours du temps.

**Critère(s) d'évaluation :**

## Présentation structurée des résultats

Les résultats de la simulation montrent que la diversité des opinions politiques diminue au fil du temps dans chaque famille, et que cette tendance s'accentue avec le temps.
![image](./src/C1.jpeg)
![image](./src/C2.jpeg)
![image](./src/C3.jpeg)

Problèmes éventuels:

- Familles de même taille
- Gestion des opinions très simplifiée
- Manque de hiérarchie dans l’influence
- Répartitions des âges irréaliste

## Lien vers page de blog : <a href="./src/accueil.html"> C'est ici ! </a>

## Bibliographie :

**Carte mentale de vos mots-clés, en utilisant** <a href="https://framindmap.org/mindmaps/index.html">Framindmap </a> 

![image](./src/mind_map.png)


1. Broutelle, Anne-Cécile. « La politique, une affaire de famille(s)? », Idées économiques et sociales, vol. 166, no. 4, 2011, pp. 31-38. Lien.

2. Cipriani, Marco, et al. « Like Mother Like Son? Experimental Evidence on
the Transmission of Values from Parents to Children ». SSRN Electronic Journal,
2007. DOI.org(Crossref). Lien

3. Commaille, Jacques. « Famille et politique ». Revue française de science politique, vol. 42, nᵒ 3, 1992, p. 485‑91. JSTOR. Lien

4. Ditmars, Van, et Mathilde Maria. Family and Politics : The Enduring Influence of the Parental Home in the Development and Transmission of Political Ideology. 2017. cadmus.eui.eu. Lien
 
5.  Family and Politics : The Enduring Influence of the Parental Home in the Development and Transmission of Political Ideology. 2017. cadmus.eui.eu. Lien

6. Hooghe, Marc. « Political Socialization and the Future of Politics ». Acta Politica, vol. 39, nᵒ 4, décembre 2004, p. 331‑41. DOI.org (Crossref). Lien
7. Huckestein, Hailey L., et al. « Major factors in the development of political attitudes ». Learning and Teaching, vol. 11, nᵒ 3, décembre 2018, p. 25‑48. DOI.org (Crossref). Lien
 
8. Ivaldi, Enrico, et al. « An Indicator for the Measurement of Political Participation: The Case of Italy ». Social Indicators Research, vol. 132, nᵒ 2, juin 2017, p. 605‑20. DOI.org (Crossref) Lien
9. Muxel, Anne. “LA POLITISATION PAR L’INTIME: PARLER POLITIQUE AVEC SES PROCHES.” Revue Française de Science Politique, vol. 65, no. 4, 2015, pp. 541–62. JSTOR, http://www.jstor.org/stable/24769675. Accessed 4 Apr. 2023. Lien
 
10. Padioleau, Jean-G., et al. « Études de socialisation politique ». Revue Française de Sociologie, vol. 11, nᵒ 1, janvier 1970, p. 84. DOI.org (Crossref). Lien
 
11. Parsons, Talcott, et al. Family, Socialization and Interaction Process. Taylor & Francis, 2007.
 
12. Peterson, James L., et Nicholas Zill. « Marital Disruption, Parent-Child Relationships, and Behavior Problems in Children ». Journal of Marriage and the Family, vol. 48, nᵒ 2, mai 1986, p. 295. DOI.org (Crossref), Lien
 
13. Schochet, Gordon J. The authoritarian family and political attitudes in 17th- century England: patriarchalism in political thought. Transaction Books, 1988.


## Travail effectué 

=> Description hebdomadaire du travail effectué (variez les auteurs !)

### Semaine 1

Nous nous sommes mis d’accord sur nos objectifs exacts pour ce projet. Nous avons retenue 
la problématique suivante : “Quelles sont les dynamiques d’opinions politiques au sein des 
familles? “. On s’intéresse à la dynamique de la transmission d’opinions politiques entre 
membres d’une même famille, influencées par des facteurs extérieurs (autres individus, 
médias...) 
Voici les pistes que nous envisageons pour la première version : 
•Niveau d’étude plutôt microscopique (nombre petit de familles) 
•L’enfant peut difficilement changer l’avis politique de ses parents 
•Le taux d’influençabilité et la capacité d’influence sont des fonctions de l’âge de 
l’individu. 
Notre objectif pour la semaine à venir est de commencer à travailler sur le code et enrichir la 
bibliographie.

### Semaine 2

Cette semaine nous avons pas mal avancé sur notre projet, du côté bibliographie et du 
côté « code ». 
Grâce à des bibliothèques numériques telles que Primo et JSTOR on a pu trouver 12 
références pour guider notre projet. Entre elles sont : des articles, des revues et des 
livres. Nous avons ciblé spécifiquement trois références, une que nous avons rejetée et 
deux que nous estimons fiables. Pour tester la fiabilité de ces dernières nous nous 
sommes basés sur différents critères, tels que les auteurs/autrices, la provenance et le 
contenu. 
En termes de code nous avons mis en place le modèle et géré la gestion de la 
vulnérabilité à l’influence d’un individu en fonction de l’âge, le taux variant de 0.1 à 0.9. 
Par exemple, le taux de vulnérabilité d’une personne 5 ans est de 0.566 tandis que 
celui d’une personne de 35 ans est de 0.108. Nous avons aussi défini la classe 
« Personne » qui est caractérisée par 5 variables. De plus on a des fonctions 
d’initialisation et des tests (à finir). 
Cette semaine qui suit nous allons finaliser le carnet de de bord. Il nous manque juste 
vérifier encore une fois nos sources et nos citations. En termes de code, on veut 
clôturer cette première version pour continuer de l’optimiser pendant la semaine 
d’après.

### Semaine 3

Nous avons finalisé l’écriture de notre carnet de bord, dans lequel nous avons cité 14 sources. 
Une parmi ces dernière a été rejetée par manque de fiabilité. C’est le livre de Nicolas Sarkozy, « 
La France pour la Vie ». Plon, 2016.  Voici notre justification : 
“Le livre "La France pour la vie" de Nicolas Sarkozy a été consulté au cours de notre recherche 
via Google, mais il a été rejeté en raison du manque d'expertise de l'auteur en sciences 
politiques et en sociologie.  
De plus, bien que le livre aborde plusieurs sujets tels que la politique économique, la sécurité, 
l'immigration, l'éducation et la culture, il ne présente pas une analyse scientifique approfondie 
des dynamiques de la transmission des opinions politiques au sein des familles. En d’autres 
termes, le livre "La France pour la vie" n'est qu'une brève mention de l'importance de la valeur 
républicaine et de la culture française dans la transmission intergénérationnelle.  
Après des recherches supplémentaires, il a été constaté que le livre avait été critiqué pour 
certaines inexactitudes et déclarations controversées, ce qui a conduit à son exclusion de 
notre bibliographie. “ 


### Semaine 4

Cette semaine nous avons réglé ce qui concerne les représentations graphiques de notre 
modèles. Nous avons plusieurs courbes qui expliquent nos résultats. On a aussi décidé 
d’introduire une attaque à notre modèle pour tester sa précision.  Actuellement on finalise les 
slides pour la soutenance.

<a href="index.html"> Retour à la page principale </a>
